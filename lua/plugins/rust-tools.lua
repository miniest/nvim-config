local rt = require("rust-tools")

rt.setup({
  tools = {
    hover_actions = { auto_focus = true },
  },
  server = {
    on_attach = function(_, bufnr)
      -- Hover actions
      vim.keymap.set("n", "<Leader>h", rt.hover_actions.hover_actions, { buffer = bufnr })
      -- Code action groups
      vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
      vim.keymap.set('n', '<Leader>e', vim.diagnostic.open_float, {noremap = true})
    end,
  },
})
