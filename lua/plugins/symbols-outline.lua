local so = require("symbols-outline")

so.setup()

local keymap = vim.api.nvim_set_keymap
keymap('n', '<leader>O', ':SymbolsOutline<CR>', {noremap = true})
