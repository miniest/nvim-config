-- install packer if not installed
local fn = vim.fn
local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
	Packer_bootstrap = fn.system({
    'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim',
		install_path
	})
end

return require('packer').startup(function(use)
  use { 'wbthomason/packer.nvim' }
  use { 'nvim-lua/plenary.nvim' }
  use { 'folke/tokyonight.nvim' } -- color scheme
  use { 'EdenEast/nightfox.nvim' }
  use { 'ellisonleao/glow.nvim', config = function() require("glow").setup() end}

  use { 'nvim-lualine/lualine.nvim',
    config = [[ require('plugins/lualine') ]]
  }
  --lsp config
  use {
    'neovim/nvim-lspconfig',
    requires = {
      {
        "williamboman/mason.nvim",
        requires = {{
          'williamboman/mason-lspconfig.nvim', 
        }},
        config = [[ require("plugins/mason-lspconfig") ]], 
      },
      { -- Standalone UI for nvim-lsp progress
        'j-hui/fidget.nvim',
        tag = 'legacy',
        event = 'BufRead',
        config = [[ require('plugins/fidget') ]]
      },
    },
    config = [[ require('plugins/lspconfig') ]],
  }
  use {
    'simrat39/rust-tools.nvim',
    after = { 'nvim-lspconfig' },
    config = [[ require('plugins/rust-tools') ]],
  }
  use { -- A completion plugin for neovim coded in Lua.
    'hrsh7th/nvim-cmp',
--    event = 'InsertEnter',
    requires = {
      { 'hrsh7th/cmp-nvim-lsp', after='nvim-cmp', }, -- nvim-cmp source for neovim builtin LSP client
      { 'hrsh7th/cmp-nvim-lua', after='nvim-cmp', }, -- nvim-cmp source for nvim lua
      { 'hrsh7th/cmp-buffer', after='nvim-cmp', }, -- nvim-cmp source for buffer words.
      { 'hrsh7th/cmp-path', after='nvim-cmp', }, -- nvim-cmp source for filesystem paths.
      { 'L3MON4D3/LuaSnip', after='nvim-cmp', },
      { 'saadparwaiz1/cmp_luasnip', after='nvim-cmp', },
    },
    config = [[ require('plugins/cmp') ]]
  }
  use { -- Nvim Treesitter configurations and abstraction layer
    'nvim-treesitter/nvim-treesitter',
    run = function() vim.cmd([[TSUpdate]]) end,
    config = [[ require('plugins/treesitter') ]]
  }
  use {
    'nvim-tree/nvim-tree.lua',
    requires = {
      'nvim-tree/nvim-web-devicons', -- optional
    },
    config = [[ require('plugins/tree') ]]
  }
  use { 'gpanders/editorconfig.nvim' }
  use {
    'zbirenbaum/copilot.lua',
    config = [[ require('plugins/copilot') ]]
  }
  use {
    'zbirenbaum/copilot-cmp',
  after = { "copilot.lua", "nvim-cmp" },
  config = [[ require('plugins/copilot-cmp') ]]
  }
  use { 
    'simrat39/symbols-outline.nvim',
    config = [[ require('plugins/symbols-outline') ]]
  }

end)
