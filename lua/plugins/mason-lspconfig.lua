require("mason").setup()

local opts = {
  ensure_installed = { 'rust_analyzer', 'yamlls', 'jsonls', 'tsserver'  }
}

require("mason-lspconfig").setup(opts)
