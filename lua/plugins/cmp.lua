local cmp = require('cmp')

cmp.setup({
  snippet = {
      expand = function(args)
        require'luasnip'.lsp_expand(args.body)
      end
    },
  sources = {
    { name = 'buffer' },
    { name = 'path' },
    { name = "nvim_lsp" },
    { name = "copilot" },
    { name = 'luasnip' },
  },
  window = {
    completion = cmp.config.window.bordered(),
    documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['C-f'] = cmp.mapping.close(),
      }),
})
