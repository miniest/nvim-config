vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

vim.opt.termguicolors = true
local tree = require('nvim-tree')

tree.setup({
  sort_by = "case_sensitive",
  diagnostics = {
    enable = true,
    show_on_dirs = true,
  },
})

local keymap = vim.api.nvim_set_keymap
keymap('n', '<leader>F', ':NvimTreeToggle<CR>', {noremap = true})
